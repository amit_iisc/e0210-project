package e0210;

import java.io.File;
import java.io.FileInputStream;

/*
 * @author Sridhar Gopinath		-		g.sridhar53@gmail.com
 * 
 * Course project,
 * Principles of Programming Course, Fall - 2016,
 * Computer Science and Automation (CSA),
 * Indian Institute of Science (IISc),
 * Bangalore
 */

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.AbstractMap;
import java.util.AbstractMap.SimpleEntry;

import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;


public class ProcessOutput {
	
	static StringBuilder output = new StringBuilder();

	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws IOException, ClassNotFoundException {

		String project = args[0];
		String testcase = args[1];

		String inPath = "Testcases/" + project + "/output/" + testcase;
		String outPath = "Testcases/" + project + "/processed-output/" + testcase;

		System.out.println("Processing " + testcase + " of " + project);

		// Read the contents of the output file into a string
		String in = new String(Files.readAllBytes(Paths.get(inPath)));
		//System.out.println(in);


		//Decode method indexes
		
		Map <String,String> tupleEntry = new HashMap<>();
		
		String path = ProgramConstants.TESTCASES + ProgramConstants.DEL + project + ProgramConstants.DEL + ProgramConstants.INTERMEDIATE_FILES + ProgramConstants.DEL + testcase + ProgramConstants.DEL + ProgramConstants.METHOD_INDEX;
		ObjectInputStream methodToIndexObjectStream = new ObjectInputStream(new FileInputStream(path));
		
		Map <String,Integer> methodIndexes = (Map <String,Integer>) methodToIndexObjectStream.readObject();
		methodToIndexObjectStream.close();
		
		System.out.println(methodIndexes.toString());
		
		//decode method graphs
		String baseDir = ProgramConstants.TESTCASES + ProgramConstants.DEL + project + ProgramConstants.DEL + ProgramConstants.INTERMEDIATE_FILES + ProgramConstants.DEL + testcase;
		
		
		Scanner sc = new Scanner(in);
		sc.useDelimiter("\\n");
		
		while(sc.hasNext()){

			String singleTuple = sc.next();
			Scanner s = new Scanner(singleTuple);
			s.useDelimiter(",");
			
		    int methodOffset = Integer.parseInt(s.next());
		    String threadId = s.next();
		    int invoc_count = Integer.parseInt(s.next());
		    String pathSeq = s.next();
		    
			output.append(methodOffset + "," + threadId + "," + invoc_count);
			Scanner sc2 = new Scanner(pathSeq);
			sc2.useDelimiter(" ");
			StringBuilder blockSeq = new StringBuilder();
			while (sc2.hasNextInt()) {
				int pathId = sc2.nextInt();
				int index = getMethodIndex(pathId, methodIndexes);
				int realPathId = pathId - index;
				File methodFile = new File(baseDir + ProgramConstants.DEL + index);
				SimpleDirectedWeightedGraph<Integer, DefaultWeightedEdge> directedGraph = getMethodGraph(methodFile);
				blockSeq.append(getBlockNumbers(directedGraph, realPathId).toString());
				//System.out.println(realPathId);
				
			}
			sc2.close();
			output.append("," +blockSeq.toString());
			output.append("\n");
		}
		//sc.close();
		
		
//		output.deleteCharAt(output.length()-1);
//		output.deleteCharAt(0);
		// Write the contents of the string to the output file
		PrintWriter out = new PrintWriter(outPath);
		out.print(output);

		out.close();

		return;
	}
	
	
	//Covert PathId to Block Sequence
	private static StringBuilder getBlockNumbers(SimpleDirectedWeightedGraph<Integer, DefaultWeightedEdge> directedGraph,
			int realPathId) {
		// TODO Auto-generated method stub\
		StringBuilder blockSeq = new StringBuilder();
		int start = -1;
		int dest = 0;
		int max;
		while (start != (directedGraph.vertexSet().size()-2)) {
			max = 0;
			for (DefaultWeightedEdge edge : directedGraph.outgoingEdgesOf(start)) {
				if (directedGraph.getEdgeWeight(edge) <= realPathId) {
					if (directedGraph.getEdgeWeight(edge) >= max) {
						max = (int) directedGraph.getEdgeWeight(edge);
						dest = directedGraph.getEdgeTarget(edge);
					}
				}
			}
			
			realPathId = realPathId - max;
			start = dest;
			if(start != (directedGraph.vertexSet().size()-2)){
				blockSeq.append(dest+" ");
			}
		}
		return blockSeq;
		
	}
	
	
	
	//Get method Graph from file
	@SuppressWarnings("unchecked")
	private static SimpleDirectedWeightedGraph<Integer, DefaultWeightedEdge> getMethodGraph(File methodFile) {
		// TODO Auto-generated method stub
		ObjectInputStream methodGraphStream;
		SimpleDirectedWeightedGraph<Integer, DefaultWeightedEdge> directedGraph = null;
		try {
			methodGraphStream = new ObjectInputStream(new FileInputStream(methodFile));
			directedGraph = (SimpleDirectedWeightedGraph<Integer, DefaultWeightedEdge>)methodGraphStream.readObject();
			methodGraphStream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		 
		return directedGraph;
	}
	
	
	
	//Compute Offset of the Method
	private static int getMethodIndex(int pathId, Map<String,Integer> methodIndexes) {
		
		// TODO Auto-generated method stub
		List <Integer> keys = new ArrayList<>(methodIndexes.values());
		Collections.sort(keys);
		int i;
		for(i=0;i < keys.size() &&  keys.get(i) <= pathId;i++){}
		
		return keys.get(i-1);
	}

}