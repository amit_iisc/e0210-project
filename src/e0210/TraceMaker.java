package e0210;

/*
 * @author Sridhar Gopinath		-		g.sridhar53@gmail.com
 * 
 * Course project,
 * Principles of Programming Course, Fall - 2016,
 * Computer Science and Automation (CSA),
 * Indian Institute of Science (IISc),
 * Bangalore
 */

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;

import com.microsoft.z3.ArithExpr;
import com.microsoft.z3.BitVecExpr;
import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.Context;
import com.microsoft.z3.IntExpr;
import com.microsoft.z3.Model;
import com.microsoft.z3.Solver;

import java.io.File;
import java.io.FileInputStream;

import soot.Local;
import soot.PackManager;
import soot.Scene;
import soot.Transform;
import soot.Type;
import soot.Unit;
import soot.Value;
import soot.JastAddJ.DivExpr;
import soot.jimple.AssignStmt;
import soot.jimple.BinopExpr;
import soot.jimple.ConditionExpr;
import soot.jimple.Constant;
import soot.jimple.DefinitionStmt;
import soot.jimple.EqExpr;
import soot.jimple.GeExpr;
import soot.jimple.GtExpr;
import soot.jimple.IdentityRef;
import soot.jimple.IfStmt;
import soot.jimple.InstanceInvokeExpr;
import soot.jimple.InvokeExpr;
import soot.jimple.InvokeStmt;
import soot.jimple.LeExpr;
import soot.jimple.LtExpr;
import soot.jimple.MulExpr;
import soot.jimple.NeExpr;
import soot.jimple.RemExpr;
import soot.jimple.ReturnStmt;
import soot.jimple.StaticFieldRef;
import soot.jimple.StaticInvokeExpr;
import soot.jimple.Stmt;
import soot.jimple.SubExpr;
import soot.jimple.VirtualInvokeExpr;
import soot.jimple.XorExpr;
import soot.toolkits.graph.Block;
import soot.toolkits.graph.ExceptionalBlockGraph;

public class TraceMaker {

	static Map<String, Integer> startOffset = new HashMap<>();
	static Map<String, String> tuple_map = new TreeMap<>();
	static Map<Integer, Integer> invoc_count = new HashMap<>();
	static Map<String, Integer> methodIndexes = new HashMap<>();
	static Map<String, String> threadWiseSchedule = new HashMap<>();
	static int childnum = 0, eventnum = 0, localnumber, globalnum = 1;
	static Map<String, List<String>> events = new HashMap<>();
	static Map<String, String> childnumber = new HashMap<>();

	static IntExpr returnValue = null;
	static Context ctx;
	static Solver solver;
	static String testcaseArgs = null;

	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws IOException, ClassNotFoundException {

		StringBuilder globaloutput = new StringBuilder();
		StringBuilder output = new StringBuilder();
		Map<String, String> contextmap = new HashMap<>();
		contextmap.put("model", "true");
		ctx = new Context(contextmap);
		solver = ctx.mkSolver();

		String project = args[0];
		String testcase = args[1];

		// Input args that were given to the testcase executable.
		// These may be need during symbolic execution
		String argsFile = "Testcases/" + project + "/input/" + testcase;
		testcaseArgs = new String(Files.readAllBytes(Paths.get(argsFile)));

		System.out.println(testcaseArgs);

		// The raw output from instrumented code. You'll use this to construct
		// the tuple for each method call
		String inPath = "Testcases/" + project + "/output/" + testcase;

		// The output files for global trace and tuples. You'll output the
		// results to these files
		String globalTraceOutPath = "Testcases/" + project + "/processed-output/" + testcase + ".global_trace";
		String tupleOutPath = "Testcases/" + project + "/processed-output/" + testcase + ".tuples";

		System.out.println("Processing " + testcase + " of " + project);

		// Read the contents of the output file into a string
		String in = new String(Files.readAllBytes(Paths.get(inPath)));

		/*
		 * 
		 * Write your algorithm which does the post-processing of the output to
		 * construct the tuple for each method call
		 * 
		 */

		// POST PROCESSING OF THE OUTPUT FILE ----- WRITING OF TUPLES

		String path = ProgramConstants.TESTCASES + ProgramConstants.DEL + project + ProgramConstants.DEL
				+ ProgramConstants.INTERMEDIATE_FILES + ProgramConstants.DEL + testcase + ProgramConstants.DEL
				+ ProgramConstants.METHOD_INDEX;
		ObjectInputStream methodToIndexObjectStream = new ObjectInputStream(new FileInputStream(path));

		methodIndexes = (Map<String, Integer>) methodToIndexObjectStream.readObject();
		methodToIndexObjectStream.close();

		System.out.println(methodIndexes.toString());

		// decode method graphs
		String baseDir = ProgramConstants.TESTCASES + ProgramConstants.DEL + project + ProgramConstants.DEL
				+ ProgramConstants.INTERMEDIATE_FILES + ProgramConstants.DEL + testcase;

		Scanner tuple_all = new Scanner(in);
		tuple_all.useDelimiter("\\n");

		//ITERATE TUPLES ONE BY ONE
		while (tuple_all.hasNext()) {

			String []singleTuple = tuple_all.next().split(",");

			// DISMANTLE TUPLES
			int methodOffset = Integer.parseInt(singleTuple[0]);
			String threadId = singleTuple[1];
			int invoc_count = Integer.parseInt(singleTuple[2]);
			String pathSeq = singleTuple[3];

			output.append(methodOffset + "," + threadId + "," + invoc_count);
			Scanner path_id = new Scanner(pathSeq);
			path_id.useDelimiter(" ");
			StringBuilder blockSeq = new StringBuilder();
			
			// FIND BLOCKS FOR EVERY PATHID
			while (path_id.hasNextInt()) {
				int pathId = path_id.nextInt();
				int index = getMethodIndex(pathId, methodIndexes);
				int realPathId = pathId - index;
				File methodFile = new File(baseDir + ProgramConstants.DEL + index);
				SimpleDirectedWeightedGraph<Integer, DefaultWeightedEdge> directedGraph = getMethodGraph(methodFile);
				blockSeq.append(getBlockNumbers(directedGraph, realPathId).toString());
				
			}
			path_id.close();
			output.append("," + blockSeq.toString());
			output.append("\n");
		}
		tuple_all.close();

		PrintWriter tupleWriter = new PrintWriter(tupleOutPath);
		tupleWriter.print(output);

		tupleWriter.close();

		// Call to the symbolic execution.
		// You can pass any data that's required for symbolic execution
		// Example: the tuples for each method call
		sootMainSymbolicExecution(project, testcase);

		
		in = new String(Files.readAllBytes(Paths.get(tupleOutPath)));

		// CREATE A MAP FOR INFORMATION OF ALL THE TUPLES
		tuple_map = createMapping(in, startOffset);

		for (String key : tuple_map.keySet()) {
			String m_sign = new String();
			for (String methd : methodIndexes.keySet()) {
				if (methodIndexes.get(methd).equals(Integer.parseInt(key.split("-")[1]))) {
					m_sign = methd;
					break;
				}
			}

			System.out.print(key.split("-")[0] + " =>=>=> ");
			System.out.print(m_sign + " =>>>>>>>> ");
			System.out.print(key.split("-")[2] + " =>>>>>> " + tuple_map.get(key));
			System.out.println();
			System.out.println("\n====================================================================================================");
		}

		// for (String key1 : map.keySet()) {
		// System.out.println("--------------" + key1 + "------------------");
		// for (int i = 0; i < 15; i++) {
		// System.out.println(i + "-----" + map.get(key1)[i]);
		// }
		// }

		for (String threadId : startOffset.keySet()) {

			int start_offset = startOffset.get(threadId);// Integer.parseInt(sc.next());
			String startmethod_Signature = new String();
			String clinitMethod_Signature = new String();
			
			invoc_count.clear();

			for (String signature : methodIndexes.keySet()) {
				if (methodIndexes.get(signature) == start_offset) {
					startmethod_Signature = signature;
				}
				if (signature.contains("clinit")) {
					clinitMethod_Signature = signature;
				}
				invoc_count.put(methodIndexes.get(signature), 0);
			}

			childnum = 0;
			eventnum = 1;
			localnumber = 1;

			events.put(threadId, new ArrayList<String>());
			events.get(threadId).add("O_" + threadId + "_" + eventnum);
			threadWiseSchedule.put("O_" + threadId + "_" + eventnum, new String(threadId + ",Begin"));
			eventnum++;

			System.out.println(
					"===========================" + "Thread " + threadId + "===================================");
			if (threadId.equals("0")) {
				traversePath(threadId, clinitMethod_Signature,new ArrayList<IntExpr>());
			}
			traversePath(threadId, startmethod_Signature,new ArrayList<IntExpr>());

			events.get(threadId).add("O_" + threadId + "_Last");
			threadWiseSchedule.put("O_" + threadId + "_Last", new String(threadId + ",End"));

		}

		for (String key : threadWiseSchedule.keySet()) {

			System.out.println(key + "  ====>  " + threadWiseSchedule.get(key));

		}

		// PROGRAM ORDER CONSTRAINTS
		for (String t_current : events.keySet()) {
			List<String> order_var = events.get(t_current);
			for (int i = 1; i < order_var.size(); i++) {
				solver.add(ctx.mkLt(ctx.mkIntConst(order_var.get(i - 1)), ctx.mkIntConst(order_var.get(i))));
			}
		}

		// FORK JOIN CONSTRAINTS
		for (String t_current : events.keySet()) {
			List<String> order_var = events.get(t_current);
			for (int i = 0; i < order_var.size(); i++) {
				String scheduleVar = threadWiseSchedule.get(order_var.get(i));
				if (scheduleVar.contains("Fork")) {
					solver.add(ctx.mkLt(ctx.mkIntConst(order_var.get(i)),
							ctx.mkIntConst("O_" + scheduleVar.substring(scheduleVar.lastIndexOf(",") + 1) + "_1")));
				}
				if (scheduleVar.contains("Join")) {
					solver.add(ctx.mkLt(
							ctx.mkIntConst("O_" + scheduleVar.substring(scheduleVar.lastIndexOf(",") + 1) + "_Last"),
							ctx.mkIntConst(order_var.get(i))));
				}
			}
		}

		// Lock Unlock Constraints
		
		for (int t_current = 0;t_current < events.keySet().toArray().length;t_current++) {
			List<String> order_var = events.get((String)events.keySet().toArray()[t_current]);

			for (int curr_lock = 0; curr_lock < order_var.size(); curr_lock++) {
				String scheduleVar = threadWiseSchedule.get(order_var.get(curr_lock));

				String lock_event = null;
				String unlock_event = null;
				String lock_event2 = null;
				String unlock_event2 = null;

				String[] eventName = scheduleVar.split(",");
				String lock_obj = null;
				if (eventName[1].equals("Lock")) {
					lock_obj = eventName[2];
					lock_event = order_var.get(curr_lock);

					for (int curr_unlock = curr_lock + 1; curr_unlock < order_var.size(); curr_unlock++) {
						if (threadWiseSchedule.get(order_var.get(curr_unlock)).split(",")[1].equals("Unlock")
								&& (lock_obj
										.equals(threadWiseSchedule.get(order_var.get(curr_unlock)).split(",")[2]))) {
							unlock_event = order_var.get(curr_unlock);
							break;
						}
					}
					
					for (int t_others = t_current + 1; t_others < events.keySet().toArray().length; t_others++) {

						List<String> order_var2 = events.get((String) events.keySet().toArray()[t_others]);
						for (int curr_lock2 = 0; curr_lock2 < order_var2.size(); curr_lock2++) {
							String scheduleVar2 = threadWiseSchedule.get(order_var2.get(curr_lock2));
							String[] eventName2 = scheduleVar2.split(",");
							if (eventName2[1].equals("Lock") && eventName2[2].equals(lock_obj)) {

								lock_event2 = order_var2.get(curr_lock2);
								for (int curr_unlock2 = curr_lock2 + 1; curr_unlock2 < order_var2
										.size(); curr_unlock2++) {
									if (threadWiseSchedule.get(order_var2.get(curr_unlock2)).split(",")[1]
											.equals("Unlock")
											&& lock_obj.equals(threadWiseSchedule.get(order_var2.get(curr_unlock2))
													.split(",")[2])) {
										unlock_event2 = order_var2.get(curr_unlock2);
										
										solver.add(ctx.mkOr(
												ctx.mkLt(ctx.mkIntConst(unlock_event2), ctx.mkIntConst(lock_event)),
												ctx.mkLt(ctx.mkIntConst(unlock_event), ctx.mkIntConst(lock_event2))));
										curr_lock2 = curr_unlock2;
										//flag = true;
										break;
									}
								}
								
								
							}

						}
					}

				}
			}

		}

		// READ WRITE CONSTRAINTS

		for (String t_current : events.keySet()) {
			List<String> order_var = events.get(t_current);
			for (int access_read = 0; access_read < order_var.size(); access_read++) {

				String scheduleVar = threadWiseSchedule.get(order_var.get(access_read));
				String[] eventVar = scheduleVar.split(",");
				if (eventVar[1].equals("Read")) {

					String symbolicName = eventVar[3];
					String o_var_read = order_var.get(access_read);
					String read_obj = eventVar[2];

					List<String> writeOrderedVars = new ArrayList<>();
					for (String t_other : threadWiseSchedule.keySet()) {

						String scheduleVar2 = threadWiseSchedule.get(t_other);
						String[] eventVar2 = scheduleVar2.split(",");

						if (eventVar2[1].equals("Write")) {
							String write_obj = eventVar2[2];
							if (read_obj.equals(write_obj)) {
								writeOrderedVars.add(t_other);
							}
						}
					}

					List<BoolExpr> rw_cnstr = new ArrayList<>();

					for (int accesswrite_curr = 0; accesswrite_curr < writeOrderedVars.size(); accesswrite_curr++) {

						BoolExpr rw_constr_pairwise = null;
						String writeValue = threadWiseSchedule.get(writeOrderedVars.get(accesswrite_curr)).split(",")[3];
						if (writeValue.matches("[0-9]+"))
							rw_constr_pairwise = ctx.mkEq(ctx.mkIntConst(symbolicName), ctx.mkInt(Integer.parseInt(writeValue)));
						else
							rw_constr_pairwise = ctx.mkEq(ctx.mkIntConst(symbolicName), ctx.mkIntConst(writeValue));

						rw_constr_pairwise = ctx.mkAnd(rw_constr_pairwise,
								ctx.mkGt(ctx.mkIntConst(o_var_read), ctx.mkIntConst(writeOrderedVars.get(accesswrite_curr))));

						for (int accesswrite_other = 0; accesswrite_other < writeOrderedVars.size(); accesswrite_other++) {
							if (accesswrite_other == accesswrite_curr)
								continue;

							rw_constr_pairwise = ctx.mkAnd(rw_constr_pairwise,
									(ctx.mkOr(
											ctx.mkGt(ctx.mkIntConst(writeOrderedVars.get(accesswrite_other)),
													ctx.mkIntConst(o_var_read)),
											ctx.mkGt(ctx.mkIntConst(writeOrderedVars.get(accesswrite_curr)),
													ctx.mkIntConst(writeOrderedVars.get(accesswrite_other))))));
						}

						rw_cnstr.add(rw_constr_pairwise);
					}
					BoolExpr rw_cnstrFinalExpr = rw_cnstr.get(0);
					for (int j = 1; j < rw_cnstr.size(); j++) {
						rw_cnstrFinalExpr = ctx.mkOr(rw_cnstrFinalExpr, rw_cnstr.get(j));
					}
					solver.add(rw_cnstrFinalExpr);
				}
			}
		}

		System.out.println(solver.toString());
		System.out.println(solver.check());
		System.out.println(solver.getAssertions().length);
		List<String> result = new ArrayList<>();
		
		//SOLVE CONSTRAINTS
		Model model = solver.getModel();
		for (String t_id : events.keySet()) {
			List<String> t_orderedVar = events.get(t_id);
			for (String orderedVar : t_orderedVar) {
				result.add(orderedVar + ":"
						+ Integer.parseInt(model.evaluate(ctx.mkIntConst(orderedVar), true).toString()));
			}
		}
		
		//SORT ORDEREDVARS ACCORDING TO SOLUTION
		for (int i = 0; i < result.size(); i++) {
			int min = i;
			String temp;
			for (int j = i; j < result.size(); j++) {
				if (Integer.parseInt(result.get(j).split(":")[1]) < Integer.parseInt(result.get(min).split(":")[1]))
					min = j;
			}
			temp = result.get(i);
			result.set(i, result.get(min));
			result.set(min, temp);
		}
		
		for(String str:result){
			System.out.println(str.split(":")[1]+"===>"+str.split(":")[0]);
		}

		for (String str : result) {
			String[] outputFormatter = threadWiseSchedule.get(str.split(":")[0]).split(",");
			for (int i = 0; i <= 2 && i < outputFormatter.length; i++) {
				globaloutput.append(outputFormatter[i] + ", ");
			}
			globaloutput.delete(globaloutput.length() - 2, globaloutput.length() - 1);
			globaloutput.append("\n");
		}
		System.out.println(globaloutput);
		// Model model = solver.getModel();

		/*
		 * You should have the intra-thread trace for each thread and the path
		 * constraints by now. Assign an order variable of the form O_i_j to the
		 * jth trace entry of the ith thread. Use the intra-thread traces to
		 * construct read-write constraints, locking constraints, program order
		 * constraints, must-happen-before constraints. These constraints will
		 * be in terms of the order variables. Put all these constaints together
		 * into one big equation: All_constraints = (Read-write constraints) ^
		 * (Path constraints) ^ (Program order constraints) ^ (Must happen
		 * before constraints) ^ (Locking constraints)
		 * 
		 */

		/*
		 * Solve the constraints using Z3 solver The solver will provide you a
		 * feasible assignment of order variables Using these values, construct
		 * your global trace To construct the global trace, you just need to put
		 * the intra-thread trace entries in ascending order of their order
		 * variables.
		 */

		// Output the global trace
		PrintWriter globalTraceWriter = new PrintWriter(globalTraceOutPath);
		globalTraceWriter.print(globaloutput);

		globalTraceWriter.close();

		return;
	}

	private static void traversePath(String threadId, String methodSignature,List<IntExpr> parameters) {
		// TODO Auto-generated method stub

		Map<String, String> lastWriteLocal = new HashMap<>();
		
		ExceptionalBlockGraph blockGraph = new ExceptionalBlockGraph(
				Scene.v().getMethod(methodSignature).retrieveActiveBody());

		int offset = methodIndexes.get(methodSignature);

		//System.out.println(Scene.v().getMethod(methodSignature).getDeclaringClass().getShortName());
		//System.out.println(Scene.v().getMethod(methodSignature).getDeclaringClass().getName());
		System.out.println(threadId + "----->" + methodSignature);
		//System.out.println(Scene.v().getMethod(methodSignature).getSubSignature());

		String[] seqOfBlocks = tuple_map.get(threadId + "-" + offset + "-" + invoc_count.get(offset)).split(" ");
		invoc_count.put(offset, invoc_count.get(offset)+1); 
		 
		for (int i = 0; i < seqOfBlocks.length; i++) {
			Block b = blockGraph.getBlocks().get(Integer.parseInt(seqOfBlocks[i]));
			//System.out.println("Traversing Block " + b.getIndexInMethod() );
			
			Unit s = b.getHead();
			int arg_index = 0;
			
			do {
				//System.out.println(s.toString());

				if (s instanceof IfStmt) {

					IfStmt stmt = (IfStmt) s;
					ConditionExpr expr = (ConditionExpr) stmt.getCondition();
					ArithExpr expr1 = null;
					ArithExpr expr2 = null;
					BoolExpr finalExpr = null;

					//EXPR FOR LEFT SIDE
					if (expr.getOp1() instanceof Local)
						expr1 = ctx.mkIntConst(lastWriteLocal.get(((Local)expr.getOp1()).getName()));
					else
						expr1 = ctx.mkInt(Integer.parseInt(expr.getOp1().toString()));
					
					//EXPR FOR RIGHT SIDE
					if (expr.getOp2() instanceof Local)
						expr2 = ctx.mkIntConst(lastWriteLocal.get(((Local)expr.getOp2()).getName()));
					else
						expr2 = ctx.mkInt(Integer.parseInt(expr.getOp2().toString()));
					
					//MAKING EXPRESSION ACCORDING TO IF CONDITION
					if (expr instanceof EqExpr)
						finalExpr = ctx.mkEq(expr1, expr2);

					else if (expr instanceof NeExpr)
						finalExpr = ctx.mkNot(ctx.mkEq(expr1, expr2));

					else if (expr instanceof GeExpr)
						finalExpr = ctx.mkGe(expr1, expr2);

					else if (expr instanceof LeExpr)
						finalExpr = ctx.mkLe(expr1, expr2);

					else if (expr instanceof GtExpr)
						finalExpr = ctx.mkGt(expr1, expr2);

					else if (expr instanceof LtExpr)
						finalExpr = ctx.mkLt(expr1, expr2);

					// CONSTRAINT PASSING BASED ON TRUE OR FALSE 
					if (stmt.getTarget().equals(blockGraph.getBlocks().get(Integer.parseInt(seqOfBlocks[i+1])).getHead())) {
						System.out.println("true part");
						solver.add(finalExpr);

					} else {
						System.out.println("false part");
						solver.add(ctx.mkNot(finalExpr));
					}
					if(solver.getAssertions().length != 0)
						System.out.println(solver.getAssertions()[solver.getAssertions().length-1]);
				}

				else if (s instanceof DefinitionStmt) {
					DefinitionStmt stmt = (DefinitionStmt) s;

					ArithExpr expr1 = null;
					ArithExpr expr2 = null;

					if (checkType(stmt.getLeftOp().getType())) {
						
						if(stmt.getRightOp() instanceof IdentityRef){
							
							expr2 = parameters.get(arg_index);
							System.out.println("IDENTITY PARAMETERS Handled with no error");
						}
						//RIGHT SIDE OF ASSIGNMENT EXPR
						if (stmt.getRightOp() instanceof Local) {

							expr2 = ctx.mkIntConst(lastWriteLocal.get(((Local)stmt.getRightOp()).getName()));
						} else if (stmt.getRightOp() instanceof StaticFieldRef) {

							expr2 = ctx.mkIntConst(
									"SymVal_" + stmt.getFieldRef().getField().getName() + threadId + "_R" + globalnum);

							threadWiseSchedule.put(addAndReturnNextOrderedVar(threadId),
									new String(threadId + ",Read" + "," + stmt.getFieldRef().getField().getSignature()
											+ "," + "SymVal_" + stmt.getFieldRef().getField().getName() + threadId
											+ "_R" + globalnum));
							globalnum++;
						}
						// if (stmt.getRightOp() instanceof InstanceFieldRef) {
						// if
						// (lastWrite.containsKey(stmt.getRightOp().toString()))
						// expr2 =
						// ctx.mkIntConst(lastWrite.get(stmt.getRightOp().toString()));
						// else {
						// expr2 = ctx.mkIntConst("Instance_" + threadId + "_" +
						// offset + "_" + localnumber);
						// localnumber++;
						// }
						// System.out.println("r3");
						// }
						else if (stmt.getRightOp() instanceof Constant) {
							expr2 = ctx.mkInt(Integer.parseInt(stmt.getRightOp().toString()));

						} else if (stmt.getRightOp() instanceof BinopExpr) {
							BinopExpr binop = (BinopExpr) stmt.getRightOp();
							IntExpr rightExpr1 = null;
							IntExpr rightExpr2 = null;

							if (binop.getOp1() instanceof Local) {
								rightExpr1 = ctx.mkIntConst(lastWriteLocal.get(((Local)binop.getOp1()).getName()));

							} else {
								rightExpr1 = ctx.mkInt(Integer.parseInt(binop.getOp1().toString()));
							}

							if (binop.getOp2() instanceof Local) {
								rightExpr2 = ctx.mkIntConst(lastWriteLocal.get(((Local)binop.getOp2()).getName()));
							} else {
								rightExpr2 = ctx.mkInt(Integer.parseInt(binop.getOp2().toString()));
							}

							if (binop instanceof soot.jimple.AddExpr)
								expr2 = ctx.mkAdd(rightExpr1, rightExpr2);

							else if (binop instanceof SubExpr)
								expr2 = ctx.mkSub(rightExpr1, rightExpr2);

							else if (binop instanceof MulExpr)
								expr2 = ctx.mkMul(rightExpr1, rightExpr2);

							else if (binop instanceof DivExpr)
								expr2 = ctx.mkDiv(rightExpr1, rightExpr2);

							else if (binop instanceof RemExpr)
								expr2 = ctx.mkRem(rightExpr1, rightExpr2);

							else if (binop instanceof XorExpr) {
								BitVecExpr bvexpr1 = ctx.mkInt2BV(32, rightExpr1);
								BitVecExpr bvexpr2 = ctx.mkInt2BV(32, rightExpr2);
								expr2 = ctx.mkBV2Int(ctx.mkBVXOR(bvexpr1, bvexpr2), true);
							} else
								System.out.println("handle more expressions");
						}

						else if (stmt.getRightOp() instanceof InvokeExpr) {
							// System.out.println(stmt);
							InvokeExpr invoc_expr = (InvokeExpr) stmt.getRightOp();
							if (methodIndexes
									.containsKey(invoc_expr.getMethod().getSignature())) {
								
								parameters.clear();
								List<Value> arguments = stmt.getInvokeExpr().getArgs();
								for(Value arg:arguments){
									if(arg instanceof Local)
										parameters.add(ctx.mkIntConst(lastWriteLocal.get(((Local)arg).getName())));
									else if(arg instanceof Constant)
										parameters.add(ctx.mkInt(Integer.parseInt(arg.toString())));
								}

								traversePath(threadId, invoc_expr.getMethod().getSignature(),parameters);
								System.out.println("RETURN VALUE HANDLED WITH NO ERROR");
								
									expr2 = returnValue;
								
							} else {
								if (stmt.getRightOp() instanceof StaticInvokeExpr) {
									Value invoc_arg = ((StaticInvokeExpr) stmt.getRightOp()).getArg(0);
			
									if (checkType(((StaticInvokeExpr) stmt.getRightOp()).getArg(0).getType())) {
										if (invoc_arg instanceof Constant) {
											expr2 = ctx.mkInt(Integer.parseInt(invoc_arg.toString()));
										} else {
											expr2 = ctx.mkIntConst(lastWriteLocal.get(invoc_arg.toString()));
										}
									} else {
										int index = Integer.parseInt(
												((AssignStmt) b.getPredOf(s)).getArrayRef().getIndex().toString());
										expr2 = ctx.mkInt(Integer.parseInt(testcaseArgs.split(" ")[index]));

									}
								}
								if (stmt.getRightOp() instanceof VirtualInvokeExpr) {

									expr2 = ctx.mkIntConst(lastWriteLocal
											.get(((VirtualInvokeExpr) stmt.getRightOp()).getBase().toString()));

								}
							}
						}
						
						//LEFT OPERATOR EXPR
						if (stmt.getLeftOp() instanceof Local) {
							expr1 = ctx.mkIntConst("local_" + threadId + "_" + offset + "_" + localnumber);
							lastWriteLocal.put(stmt.getLeftOp().toString(),
									"local_" + threadId + "_" + offset + "_" + localnumber);
							localnumber++;
						} else if (stmt.getLeftOp() instanceof StaticFieldRef
								&& !stmt.getLeftOp().getType().toString().contains("lock")) {
							expr1 = ctx.mkIntConst(
									"SymVal_" + stmt.getFieldRef().getField().getName() + threadId + "_W" + globalnum);

							threadWiseSchedule.put(addAndReturnNextOrderedVar(threadId),
									new String(threadId + ",Write" + "," + stmt.getFieldRef().getField().getSignature()
											+ "," + "SymVal_" + stmt.getFieldRef().getField().getName() + threadId
											+ "_W" + globalnum));
							globalnum++;
						}
						// if (stmt.getLeftOp() instanceof InstanceFieldRef) {
						// expr1 = ctx.mkIntConst("Instance_" + threadId + "_" +
						// offset + "_" + localnumber);
						// lastWrite.put(stmt.getLeftOp().toString(),
						// "Instance_" + threadId + "_" + offset + "_" +
						// localnumber);
						// localnumber++;
						// System.out.println("l3" +
						// stmt.getLeftOp().getType());
						// }
						// System.out.println("\n\n============\n" + expr1 + " "
						// +
						// expr2 + "\n===========\n");
						
						solver.add(ctx.mkEq(expr1, expr2));
						if(solver.getAssertions().length != 0)
							System.out.println(solver.getAssertions()[solver.getAssertions().length-1]);
					}
					else
						System.out.println("NO ASSERTIONS ADDED, STATEMENT IGNORED");
				}
				else if(s instanceof ReturnStmt){
					if(((ReturnStmt)s).getOp() instanceof Local)
						returnValue = ctx.mkIntConst(lastWriteLocal.get(((Local)((ReturnStmt)s).getOp()).getName()));
					else if(((ReturnStmt)s).getOp() instanceof Constant)
						returnValue = ctx.mkInt(Integer.parseInt(((ReturnStmt)s).getOp().toString()));	
				}
				
				else if (s instanceof InvokeStmt) {
					InvokeStmt stmt = (InvokeStmt)s;
					String newmethodSignature = stmt.getInvokeExpr().getMethod().getSignature();
					if (newmethodSignature.startsWith("<test")) {
						parameters.clear();
						List<Value> arguments = stmt.getInvokeExpr().getArgs();
						for(Value arg:arguments){
							if(arg instanceof Local)
								parameters.add(ctx.mkIntConst(lastWriteLocal.get(((Local)arg).getName())));
							else if(arg instanceof Constant)
								parameters.add(ctx.mkInt(Integer.parseInt(arg.toString())));
						}
						
						if (stmt.getInvokeExpr() instanceof VirtualInvokeExpr) {
							String classname = ((InstanceInvokeExpr) stmt.getInvokeExpr()).getBase().getType()
									.toString();
							String funcname = Scene.v().getMethod(newmethodSignature).getSubSignature();
							newmethodSignature = "<" + classname + ": " + funcname + ">";
						}
							
						traversePath(threadId, newmethodSignature, parameters);
					}
					if (newmethodSignature.contains("<java.util.concurrent.locks.Lock: void lock()>")) {
						String lockName = ((Stmt) b.getPredOf(s)).getFieldRef().getField().getSignature();
						
						threadWiseSchedule.put(addAndReturnNextOrderedVar(threadId),
								new String(threadId + ",Lock" + "," + lockName));
						
					}
					if (newmethodSignature.contains("<java.util.concurrent.locks.Lock: void unlock()>")) {
						String lockName = ((Stmt) b.getPredOf(s)).getFieldRef().getField().getSignature();

						threadWiseSchedule.put(addAndReturnNextOrderedVar(threadId),
								new String(threadId + ",Unlock" + "," + lockName));
					}
					if (newmethodSignature.contains("void start()")) {
						String childThreadName = ((Local) ((VirtualInvokeExpr) ((InvokeStmt) s).getInvokeExpr())
								.getBase()).getName();
						String childThreadId = threadId + "." + childnum;

						

						childnumber.put(childThreadName, String.valueOf(childnum));
						threadWiseSchedule.put(addAndReturnNextOrderedVar(threadId),
								new String(threadId + ",Fork" + "," + childThreadId));
						childnum++;
						
					}
					if (newmethodSignature.contains("void join()")) {
						String childThreadName = ((Local) ((VirtualInvokeExpr) ((InvokeStmt) s).getInvokeExpr())
								.getBase()).getName();
						String childThreadId = threadId + "." + childnumber.get(childThreadName);

						threadWiseSchedule.put(addAndReturnNextOrderedVar(threadId),
								new String(threadId + ",Join" + "," + childThreadId));
					}
					
				}
				
				
				s = b.getSuccOf(s);
				
			} while (s != null);

		}

	}

	private static String addAndReturnNextOrderedVar(String threadId) {
		// TODO Auto-generated method stub
		String newOrderedVar = "O_" + threadId + "_" + eventnum;
		events.get(threadId).add(newOrderedVar);
		eventnum++;
		return newOrderedVar;
	}

	private static boolean checkType(Type type) {
		// TODO Auto-generated method stub

		if (type.toString().equals("java.lang.Integer") || type.toString().equals("java.lang.Boolean")
				|| type.toString().equals("java.lang.Byte") || type.toString().equals("java.lang.Long")
				|| type.toString().equals("java.lang.Character") || type.toString().equals("int")
				|| type.toString().equals("boolean") || type.toString().equals("byte") || type.toString().equals("char")
				|| type.toString().equals("long") || type.toString().equals("java.lang.Double")
				|| type.toString().equals("double")) {

			return true;
		} else
			return false;
	}

	private static Map<String, String> createMapping(String in, Map<String, Integer> startOffset)
			throws IOException {

		Map<String, String> map = new HashMap<>();

		Scanner tuplescan = new Scanner(in);
		tuplescan.useDelimiter("\\n");
		while (tuplescan.hasNext()) {
			String []tuple = tuplescan.next().split(",");

			int methodId = Integer.parseInt(tuple[0]);
			String threadId = tuple[1];
			int invoc = Integer.parseInt(tuple[2]);
			String blockSeq = tuple[3];

			startOffset.put(threadId, methodId);

			String keyForMap = new String(threadId + "-" + methodId + "-" + invoc);

			map.put(keyForMap, blockSeq);

			
		}
		tuplescan.close();
		return map;
	}

	public static void sootMainSymbolicExecution(String project, String testcase) {

		ArrayList<String> base_args = new ArrayList<String>();

		// This is done so that SOOT can find java.lang.Object
		base_args.add("-prepend-classpath");

		base_args.add("-w");

		// Consider the Main Class as an application and not as a library
		base_args.add("-app");

		// Validate the Jimple IR at the end of the analysis
		base_args.add("-validate");

		// Exclude these classes and do not construct call graph for them
		base_args.add("-exclude");
		base_args.add("jdk.net");
		base_args.add("-exclude");
		base_args.add("java.lang");
		base_args.add("-exclude");
		base_args.add("jdk.internal.*");
		base_args.add("-no-bodies-for-excluded");

		// Retain variable names from the bytecode
		base_args.add("-p");
		base_args.add("jb");
		base_args.add("use-original-names:true");

		// Output the file as .class (Java Bytecode)
		base_args.add("-f");
		base_args.add("class");

		// Add the class path i.e. path to the JAR file
		base_args.add("-soot-class-path");
		base_args.add("Testcases/" + project + "/" + project + ".jar");

		// The Main class for the application
		base_args.add("-main-class");
		base_args.add(testcase + ".Main");

		// Class to analyze
		base_args.add(testcase + ".Main");

		base_args.add("-output-dir");
		base_args.add("Testcases/" + project + "/sootBin/");

		SymbolicExecution obj = new SymbolicExecution();

		PackManager.v().getPack("wjtp").add(new Transform("wjtp.MyAnalysis", obj));

		soot.Main.main(base_args.toArray(new String[base_args.size()]));

	}

	// Covert PathId to Block Sequence
	private static StringBuilder getBlockNumbers(
			SimpleDirectedWeightedGraph<Integer, DefaultWeightedEdge> directedGraph, int realPathId) {
		// TODO Auto-generated method stub\
		StringBuilder blockSeq = new StringBuilder();
		int start = -1;
		int dest = 0;
		int max;
		while (start != (directedGraph.vertexSet().size() - 2)) {
			max = 0;
			for (DefaultWeightedEdge edge : directedGraph.outgoingEdgesOf(start)) {
				if (directedGraph.getEdgeWeight(edge) <= realPathId) {
					if (directedGraph.getEdgeWeight(edge) >= max) {
						max = (int) directedGraph.getEdgeWeight(edge);
						dest = directedGraph.getEdgeTarget(edge);
					}
				}
			}

			realPathId = realPathId - max;
			start = dest;
			if (start != (directedGraph.vertexSet().size() - 2)) {
				blockSeq.append(dest + " ");
			}
		}
		return blockSeq;

	}

	// Get method Graph from file
	@SuppressWarnings("unchecked")
	private static SimpleDirectedWeightedGraph<Integer, DefaultWeightedEdge> getMethodGraph(File methodFile) {
		// TODO Auto-generated method stub
		ObjectInputStream methodGraphStream;
		SimpleDirectedWeightedGraph<Integer, DefaultWeightedEdge> directedGraph = null;
		try {
			methodGraphStream = new ObjectInputStream(new FileInputStream(methodFile));
			directedGraph = (SimpleDirectedWeightedGraph<Integer, DefaultWeightedEdge>) methodGraphStream.readObject();
			methodGraphStream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return directedGraph;
	}

	// Compute Offset of the Method
	private static int getMethodIndex(int pathId, Map<String, Integer> methodIndexes) {

		// TODO Auto-generated method stub
		List<Integer> keys = new ArrayList<>(methodIndexes.values());
		Collections.sort(keys);
		int i;
		for (i = 0; i < keys.size() && keys.get(i) <= pathId; i++) {
		}

		return keys.get(i - 1);
	}

}
