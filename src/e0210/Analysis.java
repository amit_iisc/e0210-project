package e0210;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.jgrapht.graph.*;

/*
 * @author Amit Yadav - amitrao042@gmail.com
 * 
 * Course project,
 * Principles of Programming Course, Fall - 2016,
 * Computer Science and Automation (CSA),
 * Indian Institute of Science (IISc),
 * Bangalore
 */

import java.util.Map;

import org.jgrapht.traverse.TopologicalOrderIterator;

import soot.*;
import soot.jimple.*;
import soot.toolkits.graph.Block;
import soot.toolkits.graph.BlockGraph;
import soot.toolkits.graph.ExceptionalBlockGraph;
import soot.util.*;

public class Analysis extends BodyTransformer {

	static SootClass counterClass;
	static SootMethod reportCounter, ptc, initialize,makeTuple,printTuple;

	static {
		counterClass = Scene.v().loadClassAndSupport("e0210.T_AnalysisManager");
		ptc = counterClass.getMethod("void parentToChild(long,long)");
		initialize = counterClass.getMethod("void initialize(long)");
		reportCounter = counterClass.getMethod("int report(long,int)");
		makeTuple = counterClass.getMethod("void makeTuple(java.lang.StringBuilder,int)");
		printTuple = counterClass.getMethod("void printTuple(long,int,int,java.lang.StringBuilder)");
	}

	static int methodId = 0;
	static Map <String,Integer> methodIndexes = new HashMap<>();

	@Override
	protected synchronized void internalTransform(Body body, String phaseName, Map<String, String> options) {

		if (body.getMethod().getDeclaringClass().equals(counterClass) || body.getMethod().getDeclaringClass().toString().contains("popUtil.PoP_Util")) { //|| body.getMethod().getDeclaringClass().equals(Scene.v().loadClassAndSupport("Testcases/project-3/src/popUtil.PoP_Util"))
			return;
		}
		
		System.out.println(body.toString());

		int nameToId;
		ExceptionalBlockGraph blockGraph = new ExceptionalBlockGraph(body); 
		
		System.out.println(blockGraph.toString());

		Chain<Unit> units = body.getUnits();
		Local reg, printVar, threadVar, parentThread, currentThread,numInvoc;
		printVar = Jimple.v().newLocal("printVar", RefType.v(ProgramConstants.REF_PRINT));
		body.getLocals().add(printVar);
		reg = Jimple.v().newLocal("register", IntType.v());
		body.getLocals().add(reg);

		threadVar = Jimple.v().newLocal("threadVar", RefType.v(ProgramConstants.REF_THREAD));
		body.getLocals().add(threadVar);
		parentThread = Jimple.v().newLocal("parentThread", LongType.v());
		body.getLocals().add(parentThread);
		currentThread = Jimple.v().newLocal("currentThread", LongType.v());
		body.getLocals().add(currentThread);
		numInvoc = Jimple.v().newLocal("numInvoc", IntType.v());
		body.getLocals().add(numInvoc);

		Local methodOffset = Jimple.v().newLocal("methodOffset", IntType.v());
		body.getLocals().add(methodOffset);
		
		Local pathSeq  = Jimple.v().newLocal("pathSeq", RefType.v(ProgramConstants.REF_STRINGBUILDER));
		body.getLocals().add(pathSeq);

		Map<Integer, Integer> numPaths = new HashMap<Integer, Integer>();

		// CREATE GRAPH
		SimpleDirectedWeightedGraph<Integer, DefaultWeightedEdge> directedGraph = createDirectedGraph(blockGraph);

		// BALL LARUS ALGORITHM
		{
			List<Integer> reverseTop = reverseTopOrder(directedGraph);

			for (int i = reverseTop.size() - 1; i >= 0; i--) {
				int blockNum = reverseTop.get(i);
				if (blockNum == blockGraph.size())
					numPaths.put(blockNum, 1);
				else {
					numPaths.put(blockNum, 0);
					for (DefaultWeightedEdge edge : directedGraph.outgoingEdgesOf(blockNum)) {
						// Edge profiling

						directedGraph.setEdgeWeight(edge, numPaths.get(blockNum));
						numPaths.put(blockNum,
								numPaths.get(blockNum) + numPaths.get(directedGraph.getEdgeTarget(edge)));
					}
				}
			}
		}

		// Setting method offset
		synchronized (Analysis.class) {
			methodIndexes
					.put(body.getMethod().getSignature(),methodId);
			nameToId = methodId;
			methodId = methodId + numPaths.get(-1);
		}

		// Initialize Register  //CODE MODIFIED FOR THREAD
		{
			Unit firstStmt = units.getPredOf(((JimpleBody)body).getFirstNonIdentityStmt());
			Stmt s = Jimple.v().newAssignStmt(reg, IntConstant.v(0));
			if(firstStmt != null)
				units.insertAfter(s, firstStmt);
			else
				units.insertBefore(s, ((JimpleBody)body).getFirstNonIdentityStmt());
			//Initialize pathId and methodOffset
			units.insertBefore(Jimple.v().newAssignStmt(methodOffset, IntConstant.v(nameToId)), s);
			
			// String Builder for storing PathId's InitiAlized
			units.insertBefore(Jimple.v().newAssignStmt(pathSeq, Jimple.v().newNewExpr(RefType.v(ProgramConstants.REF_STRINGBUILDER))),s);
			units.insertBefore(Jimple.v().newInvokeStmt(Jimple.v().newSpecialInvokeExpr(pathSeq, Scene.v().getMethod(ProgramConstants.SBINIT_METHOD).makeRef())), s);
			
			// Current Thread and GetId method invocations
			AssignStmt stmt = Jimple.v().newAssignStmt(threadVar, Jimple.v()
					.newStaticInvokeExpr(Scene.v().getMethod(ProgramConstants.CURRENTTHREAD_METHOD).makeRef()));
			units.insertBefore(stmt, s);
			units.insertBefore(Jimple.v().newAssignStmt(parentThread, Jimple.v().newVirtualInvokeExpr(threadVar,
					Scene.v().getMethod(ProgramConstants.THREADID_METHOD).makeRef())), s);
			
			//Main method special handling
			if (body.getMethod().getSignature().contains("main") || body.getMethod().getSignature().contains("clinit")) {
				InvokeExpr callInitialize = Jimple.v().newStaticInvokeExpr(initialize.makeRef(),parentThread);
				Stmt callStmt = Jimple.v().newInvokeStmt(callInitialize);
				units.insertBefore(callStmt, s);
			}
			
			// Invoke report method via hook
			InvokeExpr reportExpr = Jimple.v().newStaticInvokeExpr(reportCounter.makeRef(), parentThread, methodOffset);
			Stmt reportStmt = Jimple.v().newAssignStmt(numInvoc,reportExpr);
			units.insertBefore(reportStmt, s);

		}

		// EDGE PROFILING
		for (Block src : blockGraph.getBlocks()) {
			for (Block dest : blockGraph.getUnexceptionalSuccsOf(src)) {
				// Instrumentation for Back Edges

				if (!src.toString().contains(ProgramConstants.EXIT_METHOD)) {

					if (src.getIndexInMethod() >= dest.getIndexInMethod()) {
						int valEdge = (int) directedGraph
								.getEdgeWeight(directedGraph.getEdge(src.getIndexInMethod(), blockGraph.size()));
						Unit incStmt = Jimple.v().newAssignStmt(reg,
								Jimple.v().newAddExpr(reg, IntConstant.v(valEdge)));
						int resetValue = (int) directedGraph
								.getEdgeWeight(directedGraph.getEdge(-1, dest.getIndexInMethod()));
						Unit resetReg = Jimple.v().newAssignStmt(reg, IntConstant.v(resetValue));
						units.insertOnEdge(incStmt, src.getTail(), dest.getHead());

						units.insertAfter(resetReg, incStmt);
						
		
						InvokeExpr makeTupleExpr = Jimple.v().newStaticInvokeExpr(makeTuple.makeRef(),pathSeq,reg);
						Stmt makeTupleStmt = Jimple.v().newInvokeStmt(makeTupleExpr);
						units.insertAfter(makeTupleStmt, incStmt);
						units.insertAfter(
								Jimple.v().newAssignStmt(reg, Jimple.v().newAddExpr(reg, IntConstant.v(nameToId))),
								incStmt);
					} else {

						// Instrumentation in the program
						if (directedGraph.getEdgeWeight(
								directedGraph.getEdge(src.getIndexInMethod(), dest.getIndexInMethod())) != Double
										.valueOf(0)) {

							int valEdge = (int) directedGraph.getEdgeWeight(
									directedGraph.getEdge(src.getIndexInMethod(), dest.getIndexInMethod()));
							Unit incStmt = Jimple.v().newAssignStmt(reg,
									Jimple.v().newAddExpr(reg, IntConstant.v(valEdge)));
							units.insertOnEdge(incStmt, src.getTail(), dest.getHead());
						}
					}

				}

			}
		}

		// Print on return
		Iterator<Unit> stmtIt = body.getUnits().snapshotIterator();

		while (stmtIt.hasNext()) {
			boolean isExitCall = false;
			Unit s = stmtIt.next();
			
			if (s instanceof InvokeStmt) {
				if (((InvokeStmt) s).toString().contains(ProgramConstants.EXIT_METHOD))
					isExitCall = true;
				
				//new code for thread information
				if (s.toString().contains("start")) {
					InvokeExpr expr = ((InvokeStmt) s).getInvokeExpr();
					Local rec = (Local) ((VirtualInvokeExpr) expr).getBase();
					units.insertBefore(
							Jimple.v()
									.newAssignStmt(currentThread,
											Jimple.v()
													.newVirtualInvokeExpr(rec, Scene.v()
															.getMethod(ProgramConstants.THREADID_METHOD).makeRef())),
							s);

					InvokeExpr ptcExpr = Jimple.v().newStaticInvokeExpr(ptc.makeRef(), currentThread, parentThread);
					Stmt ptcStmt = Jimple.v().newInvokeStmt(ptcExpr);
					units.insertBefore(ptcStmt, s);
				}

			}

			if ((s instanceof ReturnStmt) || (s instanceof ReturnVoidStmt) || isExitCall) {

				units.insertBefore(Jimple.v().newAssignStmt(reg, Jimple.v().newAddExpr(reg, IntConstant.v(nameToId))),
						s);

				InvokeExpr makeTupleExpr = Jimple.v().newStaticInvokeExpr(makeTuple.makeRef(), pathSeq,reg);
				Stmt makeTupleStmt = Jimple.v().newInvokeStmt(makeTupleExpr);
				units.insertBefore(makeTupleStmt, s);


				InvokeExpr printTupleExpr = Jimple.v().newStaticInvokeExpr(printTuple.makeRef(),parentThread,methodOffset,numInvoc,pathSeq);
				Stmt printTupleStmt = Jimple.v().newInvokeStmt(printTupleExpr);
				units.insertBefore(printTupleStmt, s);

			}
		}

		// Dump Graph to Files
		saveGraphToFile(directedGraph, nameToId);

		// for (DefaultWeightedEdge edge : directedGraph.edgeSet()) {
		// System.out.println(edge.toString() + " " +
		// directedGraph.getEdgeWeight(edge));
		// }
		// System.out.println(blockGraph.toString());
		// System.out.println(directedGraph.toString());

		//System.out.println(body.toString());
		
		return;
	}


	private void saveGraphToFile(SimpleDirectedWeightedGraph<Integer, DefaultWeightedEdge> methodGraph,
			int methodOffset) {
		// TODO Auto-generated method stub
		String path = new String(ProgramConstants.TESTCASES + ProgramConstants.DEL + Main.projectName
				+ ProgramConstants.DEL + ProgramConstants.INTERMEDIATE_FILES + ProgramConstants.DEL + Main.testCaseNum);
		File methodFile = new File(path + ProgramConstants.DEL + methodOffset);

		try {
			FileOutputStream methodToGraph = new FileOutputStream(methodFile, false);
			ObjectOutputStream graphMapping = new ObjectOutputStream(methodToGraph);
			graphMapping.writeObject(methodGraph);
			graphMapping.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	protected void dumpMethodIndex(String path) {
		// write graphs to File
		String file = new String(path + ProgramConstants.DEL + ProgramConstants.METHOD_INDEX);

		try {
			FileOutputStream methodIdToName = new FileOutputStream(file, false);
			ObjectOutputStream pathMapping = new ObjectOutputStream(methodIdToName);
			pathMapping.writeObject(methodIndexes);
			pathMapping.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public DefaultWeightedEdge addAndReturnEdge(SimpleDirectedWeightedGraph<Integer, DefaultWeightedEdge> graph,
			int source, int destination) {
		DefaultWeightedEdge newEdge = graph.addEdge(source, destination);
		if (newEdge == null) {
			newEdge = graph.getEdge(source, destination);
		}
		return newEdge;
	}

	// Directed Weighted Graph
	public SimpleDirectedWeightedGraph<Integer, DefaultWeightedEdge> createDirectedGraph(BlockGraph blockGraph) {

		SimpleDirectedWeightedGraph<Integer, DefaultWeightedEdge> directedGraph = new SimpleDirectedWeightedGraph<Integer, DefaultWeightedEdge>(
				DefaultWeightedEdge.class);

		// add Vertices
		for (Block b : blockGraph.getBlocks())
			directedGraph.addVertex(b.getIndexInMethod());

		// add dummy head
		int dummyHead = -1;
		directedGraph.addVertex(dummyHead);

		for (Block b : blockGraph.getHeads()) {
			directedGraph.setEdgeWeight(addAndReturnEdge(directedGraph, dummyHead, b.getIndexInMethod()),
					Double.valueOf(0));
		}

		// add dummy tail
		directedGraph.addVertex(blockGraph.size());

		for (Block b : blockGraph.getTails()) {
			directedGraph.setEdgeWeight(addAndReturnEdge(directedGraph, b.getIndexInMethod(), blockGraph.size()),
					Double.valueOf(0));
		}

		// add all edges
		for (Block b : blockGraph.getBlocks()) {

			for (Block blk : b.getSuccs()) {

				// Handle loops
				if (b.getIndexInMethod() >= blk.getIndexInMethod()) {
					directedGraph.setEdgeWeight(addAndReturnEdge(directedGraph, dummyHead, blk.getIndexInMethod()),
							Double.valueOf(0));
					directedGraph.setEdgeWeight(
							addAndReturnEdge(directedGraph, b.getIndexInMethod(), blockGraph.size()),
							Double.valueOf(0));
				}

				else {
					directedGraph.setEdgeWeight(
							addAndReturnEdge(directedGraph, b.getIndexInMethod(), blk.getIndexInMethod()),
							Double.valueOf(0));
				}
			}

			// Handle Exit Call
			if (b.toString().contains(ProgramConstants.EXIT_METHOD)) {

				List<DefaultWeightedEdge> edgesToDelete = new ArrayList<>();
				for (DefaultWeightedEdge edge : directedGraph.outgoingEdgesOf(b.getIndexInMethod())) {
					edgesToDelete.add(edge);
				}
				for (int i = 0; i < edgesToDelete.size(); i++) {
					directedGraph.removeEdge(edgesToDelete.get(i));
				}
				if (!directedGraph.containsEdge(b.getIndexInMethod(), blockGraph.size())) {
					directedGraph.setEdgeWeight(directedGraph.addEdge(b.getIndexInMethod(), blockGraph.size()),
							Double.valueOf(0));
				}
			}
		}

		return directedGraph;
	}

	public List<Integer> reverseTopOrder(org.jgrapht.DirectedGraph<Integer, DefaultWeightedEdge> directedGraph) {

		TopologicalOrderIterator<Integer, DefaultWeightedEdge> topOrder = new TopologicalOrderIterator<Integer, DefaultWeightedEdge>(
				directedGraph);

		List<Integer> reverseTop = new ArrayList<>();
		while (topOrder.hasNext()) {
			reverseTop.add(topOrder.next());
		}

		return reverseTop;

	}
}