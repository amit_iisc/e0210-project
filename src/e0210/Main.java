package e0210;

import java.io.File;
import java.io.IOException;


/*
 * @author Sridhar Gopinath		-		g.sridhar53@gmail.com
 * 
 * Course project,
 * Principles of Programming Course, Fall - 2016,
 * Computer Science and Automation (CSA),
 * Indian Institute of Science (IISc),
 * Bangalore
 */

import java.util.ArrayList;
import java.util.List;

import soot.PackManager;
import soot.Scene;
import soot.SootClass;
import soot.Transform;
import soot.options.Options;

public class Main {
	
	static String projectName;
	static String testCaseNum;

	public static void main(String[] args) throws IOException {

		String project = args[0];
		String testcase = args[1];
		
		projectName = project;
		testCaseNum = testcase;

		ArrayList<String> base_args = new ArrayList<String>();

		// This is done so that SOOT can find java.lang.Object
		base_args.add("-prepend-classpath");

		// Consider the Main Class as an application and not as a library
		base_args.add("-app");

		// Validate the Jimple IR at the end of the analysis
		base_args.add("-validate");

		// Exclude these classes and do not construct call graph for them
		base_args.add("-exclude");
		base_args.add("jdk.net");
		base_args.add("-exclude");
		base_args.add("java.lang");
		base_args.add("-no-bodies-for-excluded");

		// Retain variable names from the bytecode
		base_args.add("-p");
		base_args.add("jb");
		base_args.add("use-original-names:true");

		// Output the file as .class (Java Bytecode)
		base_args.add("-f");
		base_args.add("class");

		// Add the class path i.e. path to the JAR file
		//base_args.add("-soot-class-path");
		//configure("Testcases/" + project + "/" + project + ".jar:bin/");
		
		Options.v().set_soot_classpath("Testcases/" + project + "/" + project + ".jar:bin/");
	    Options.v().set_prepend_classpath(true);

		// The Main class for the application
		base_args.add("-main-class");
		base_args.add(testcase + ".Main");

		// Class to analyze
		base_args.add(testcase + ".Main");

		base_args.add("-output-dir");
		base_args.add("Testcases/" + project + "/sootBin/");
		
		//List <String> exclude = new ArrayList<>();
		//exclude.add("e0210.MyCounter");
		//Options.v().set_exclude(exclude);
		//create necessary files
		createFileStructure(project,testcase);
		

		Analysis obj = new Analysis();

		PackManager.v().getPack("jtp").add(new Transform("jtp.MyAnalysis", obj));
		
		Scene.v().addBasicClass("java.io.PrintStream",SootClass.SIGNATURES);
		Scene.v().addBasicClass("java.lang.System",SootClass.SIGNATURES);
		Scene.v().addBasicClass("java.lang.Thread",SootClass.SIGNATURES);

		soot.Main.main(base_args.toArray(new String[base_args.size()]));
		
		
		//dump object in file
		obj.dumpMethodIndex(ProgramConstants.TESTCASES + ProgramConstants.DEL + project + ProgramConstants.DEL + ProgramConstants.INTERMEDIATE_FILES + ProgramConstants.DEL + testcase );

		return;
	}

//	private static void configure(String path) {
//		// TODO Auto-generated method stub
//		Options.v().set_verbose(false);
//        Options.v().set_keep_line_number(true);
//        Options.v().set_src_prec(Options.src_prec_class);
//        Options.v().set_soot_classpath(path);
//        Options.v().set_prepend_classpath(true);
//	}

	private static void createFileStructure(String project,String testcase) {
		// TODO Auto-generated method stub
        String baseDirName = new String( ProgramConstants.TESTCASES + ProgramConstants.DEL + project + ProgramConstants.DEL + ProgramConstants.INTERMEDIATE_FILES);
		File baseDir = new File(baseDirName);
		// if the directory does not exist, create it
		if (!baseDir.exists()) {
			
			baseDir.mkdirs();
		}
		
		File testCaseDir = new File(baseDirName + ProgramConstants.DEL + testcase);

		if (!testCaseDir.exists()) {

			testCaseDir.mkdirs();
		}

	}
	

}