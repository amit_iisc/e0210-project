package e0210;

import java.util.HashMap;
import java.util.Map;

public class T_AnalysisManager {
	/* the counter, initialize to zero */

	static String [] tCustomId = new String[10000];
	static int [] tNumForks = new int[10000];
	@SuppressWarnings("unchecked")
	static HashMap<Integer,Integer> [] tMethodCount = new HashMap[10000];
	static Map<String, String> tupleEntry = new HashMap<>(); 
	
	public static void initialize(long threadname){
		tCustomId[(int)threadname] = "0";
		tNumForks[(int)threadname] = 0;
	}

	public static int report(long threadname, int offset) {
		
		int threadId = (int) threadname;
		int num_invoc;

		if (tMethodCount[threadId] == null) {
			HashMap<Integer, Integer> newThreadMap = new HashMap<>();
			tMethodCount[threadId] = newThreadMap;
		}

		if (!tMethodCount[threadId].containsKey(offset)) {
			num_invoc = 0;
		} 
		else {
			num_invoc = tMethodCount[threadId].get(offset) + 1;	
		}
		
		tMethodCount[threadId].put(offset, num_invoc);

		//System.out.println(tCustomId[(int)threadname] + " : " + offset + " : " +tMethodCount[(int)threadname].get(offset));
		return num_invoc;
	}
	
	public static void parentToChild(long child,long parent) {
			
		tCustomId[(int)child] = tCustomId[(int)parent] + "." + tNumForks[(int)parent];
		tNumForks[(int)child] = 0;
		tNumForks[(int)parent] += 1;
		//System.err.println(tCustomId[(int)child]);
		//System.err.println(String.valueOf(tNumForks[(int)parent]));
	}
	
	public static synchronized void makeTuple(StringBuilder pathSeq,int pathId) {
		
		pathSeq.append(" " + pathId);
		 
	}
	
	public static synchronized void printTuple(long threadName, int methodOffset, int numberOfCalls , StringBuilder pathSeq){
		
		String threadId = tCustomId[(int)threadName];
		
		System.out.print(methodOffset + ",");
		System.out.print(threadId + ",");
		System.out.print(numberOfCalls + ",");
		System.out.println(pathSeq.toString());
	}
}
