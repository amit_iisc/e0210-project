package pop_replay;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import soot.Body;
import soot.IntType;
import soot.Local;
import soot.LongType;
import soot.PatchingChain;
import soot.RefType;
import soot.Scene;
import soot.SceneTransformer;
import soot.SootClass;
import soot.SootField;
import soot.SootMethod;
import soot.Type;
import soot.Unit;
import soot.Value;
import soot.ValueBox;
import soot.jimple.AssignStmt;
import soot.jimple.IntConstant;
import soot.jimple.InvokeExpr;
import soot.jimple.InvokeStmt;
import soot.jimple.Jimple;
import soot.jimple.JimpleBody;
import soot.jimple.NullConstant;
import soot.jimple.StaticFieldRef;
import soot.jimple.Stmt;
import soot.jimple.StringConstant;
import soot.jimple.VirtualInvokeExpr;
import soot.util.Chain;

public class PoP_Replay_Instrumentor extends SceneTransformer
{
	static SootClass utilClass;
	static SootMethod criticalBefore, ptc, initialize,criticalAfter,printTuple;
	static StringBuilder bodyrecorder = new StringBuilder();

	static {
		utilClass = Scene.v().loadClassAndSupport("pop_replay.PoP_Replay_Util");
		initialize = utilClass.getMethod("void initialize()");
		ptc = utilClass.getMethod("void parentToChild(long)");
		
		criticalBefore = utilClass.getMethod("void criticalBefore()");
		criticalAfter = utilClass.getMethod("void criticalAfter()");

	}
	
    @Override
    protected synchronized void internalTransform(String arg0, Map<String, String> arg1) {
        Chain<SootClass> allClasses = Scene.v().getApplicationClasses();
        for (SootClass curClass: allClasses) {
        
            
   
        	System.out.println("CLASS   "+curClass.getName());
        	
        	/* These classes must be skipped */
            if (curClass.getName().contains("pop_replay.PoP_Replay_Util")
             || curClass.getName().contains("popUtil.PoP_Util")
             || curClass.getName().contains("jdk.internal")) {
                continue;
            }
            
            List<SootMethod> allMethods = curClass.getMethods();
            for (SootMethod curMethod: allMethods) {  
            	
            	System.out.println("iNSTRUMENTING METHOD   "+curMethod.getSignature());
            	
            	Body body = curMethod.retrieveActiveBody();
            	
            	Local childThread;

        		childThread = Jimple.v().newLocal("childThread", LongType.v());
        		body.getLocals().add(childThread);

            	Stmt first_stmt = ((JimpleBody)body).getFirstNonIdentityStmt();
           	
            	if (curMethod.getSignature().contains("main") || body.getMethod().getSignature().contains("clinit")) {
            		
    				Stmt callStmt = Jimple.v().newInvokeStmt(Jimple.v().newStaticInvokeExpr(initialize.makeRef()));
    				body.getUnits().insertBefore(callStmt, first_stmt);
    			}
            	
            	
            	Iterator<Unit> stmtIt = body.getUnits().snapshotIterator();
            	while(stmtIt.hasNext()){
            		Unit curr_unit = stmtIt.next();
            		
            		String eventName = null,objName = null;
            		
            		if(curr_unit instanceof AssignStmt){
            			
            			AssignStmt curr_stmt = (AssignStmt)curr_unit;
            			
            			if(!checkType(curr_stmt.getLeftOp().getType()))
            				continue;
            			
            			System.out.println("Instrumrnting :   "+curr_unit);
						if (curr_stmt.getRightOp() instanceof StaticFieldRef
								|| curr_stmt.getLeftOp() instanceof StaticFieldRef) {
							

							InvokeExpr cr_bf_Expr = Jimple.v().newStaticInvokeExpr(criticalBefore.makeRef());
							Stmt cr_bf_stmt = Jimple.v().newInvokeStmt(cr_bf_Expr);
							body.getUnits().insertBefore(cr_bf_stmt,curr_unit);

							 InvokeExpr cr_af_Expr = Jimple.v().newStaticInvokeExpr(criticalAfter.makeRef());
							 Stmt cr_af_Stmt = Jimple.v().newInvokeStmt(cr_af_Expr);
							 body.getUnits().insertAfter(cr_af_Stmt, curr_unit);
						}
            		}
            		else if(curr_unit instanceof InvokeStmt){
            			InvokeStmt curr_stmt = (InvokeStmt)curr_unit;
            			
            			if (curr_stmt.getInvokeExpr().toString().contains("void start()") || curr_stmt.getInvokeExpr().toString().contains("void join()")) {
        					
        					if(curr_stmt.getInvokeExpr().toString().contains("void start()")){
								Local rec = (Local) ((VirtualInvokeExpr) curr_stmt.getInvokeExpr()).getBase();
								body.getUnits().insertBefore(
										Jimple.v().newAssignStmt(childThread,
												Jimple.v().newVirtualInvokeExpr(rec, Scene.v()
														.getMethod(ProgramConstants.THREADID_METHOD).makeRef())),
										curr_unit);

								InvokeExpr ptcExpr = Jimple.v().newStaticInvokeExpr(ptc.makeRef(), childThread);
								Stmt ptcStmt = Jimple.v().newInvokeStmt(ptcExpr);
								body.getUnits().insertBefore(ptcStmt, curr_unit);
        					}
        					
        					InvokeExpr cr_bf_Expr = Jimple.v().newStaticInvokeExpr(criticalBefore.makeRef());
							Stmt cr_bf_stmt = Jimple.v().newInvokeStmt(cr_bf_Expr);
							body.getUnits().insertBefore(cr_bf_stmt,curr_unit);
        					
        					InvokeExpr cr_af_Expr = Jimple.v().newStaticInvokeExpr(criticalAfter.makeRef());
        					Stmt cr_af_Stmt = Jimple.v().newInvokeStmt(cr_af_Expr);
        					body.getUnits().insertAfter(cr_af_Stmt, curr_unit);
        				}
            		
            			else if(curr_stmt.getInvokeExpr().toString().contains("java.util.concurrent.locks") && (curr_stmt.getInvokeExpr().toString().contains("void lock") || curr_stmt.getInvokeExpr().toString().contains("void unlock"))){
            	
            				
            				InvokeExpr cr_bf_Expr = Jimple.v().newStaticInvokeExpr(criticalBefore.makeRef());
        					Stmt cr_bf_stmt = Jimple.v().newInvokeStmt(cr_bf_Expr);
        					body.getUnits().insertBefore(cr_bf_stmt, curr_unit);
        					
        					InvokeExpr cr_af_Expr = Jimple.v().newStaticInvokeExpr(criticalAfter.makeRef());
        					Stmt cr_af_Stmt = Jimple.v().newInvokeStmt(cr_af_Expr);
        					body.getUnits().insertAfter(cr_af_Stmt, curr_unit);
            			}
            			
            			
            		}
            	}
            	System.out.println(body.toString());
            }  
        }  
//        try {
//			PrintWriter bodyWriter = new PrintWriter("bodyrecorded");
//			bodyWriter.print(bodyrecorder);
//		} catch (FileNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
    }
    
    private boolean checkType(Type type) {
		// TODO Auto-generated method stub

		if (type.toString().equals("java.lang.Integer") || type.toString().equals("java.lang.Boolean")
				|| type.toString().equals("java.lang.Byte") || type.toString().equals("java.lang.Long")
				|| type.toString().equals("java.lang.Character") || type.toString().equals("int")
				|| type.toString().equals("boolean") || type.toString().equals("byte") || type.toString().equals("char")
				|| type.toString().equals("long") || type.toString().equals("java.lang.Double")
				|| type.toString().equals("double")) {

			return true;
		} else
			return false;
	}
}
