package pop_replay;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


public class PoP_Replay_Util 
{
	static String [] tCustomId = new String[10000];
	static int [] tNumForks = new int[10000];
	static boolean lineExecuted = true;
	static Lock lock = new ReentrantLock();
	static String []allTraceEvents = null; 
	static int curr_event_index = 2;
	static String  curr_event  = null;
    static {
           
            /* The global_trace will be copied to the current directory before running the test case */
    		
            try {
            	allTraceEvents = new String(Files.readAllBytes(Paths.get("global_trace"))).split("\n");
            	curr_event = allTraceEvents[1].split(",")[0];
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				System.out.println("FILE GLOBAL_TRACE NOT FOUND");
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            
    }
    
  
    public static void parentToChild(long child) throws IOException, InterruptedException {
		
    	long parent = Thread.currentThread().getId();

		tCustomId[(int) child] = tCustomId[(int) parent] + "." + tNumForks[(int) parent];
		tNumForks[(int) child] = 0;
		tNumForks[(int) parent] += 1;
		
    	
		//System.out.println(tCustomId[(int)child]);
		//System.out.println(String.valueOf(tNumForks[(int)parent]));
		
	}

    public static void initialize(){
    	long threadname = Thread.currentThread().getId();
		tCustomId[(int)threadname] = "0";
		tNumForks[(int)threadname] = 0;
	}
    
    /* You can modify criticalBefore() to receive the required arguments */
    public static void criticalBefore() throws IOException, InterruptedException {
    	
    	Thread t_curr = Thread.currentThread();
    	String threadId = tCustomId[(int)t_curr.getId()];

		while (true) {
		
			//System.out.println(nextEvent[0]+"  "+nextEvent[1]+"  "+objName);
			//System.out.println(threadId+"  "+eventType+"  "+objName);

	    	System.out.println(threadId+ " tring to execute stmt of  " + curr_event );
	    	
			if ( curr_event.equals(threadId) ) {		
				System.out.println(threadId+" executng current event");
				//System.out.println(threadId+"  "+eventType+"  "+objName);
				break;
			}else{
				synchronized (lock) {
					while(!threadId.equals(curr_event)){
						System.out.println(threadId+" entered wait");
						lock.wait();
					}
					System.out.println(threadId+" Woke up");
				}
			}
		}
    }
    
    private static String findNextEventInTrace() throws IOException {
		// TODO Auto-generated method stub
    	String line = null;
    	while(curr_event_index < allTraceEvents.length && (allTraceEvents[curr_event_index].contains("Begin") || allTraceEvents[curr_event_index].contains("End"))){
			curr_event_index++;
		}
    	System.out.println(curr_event_index +"     "+allTraceEvents.length);
    	if(curr_event_index < allTraceEvents.length){
    		line = allTraceEvents[curr_event_index];
    		curr_event_index++;
    	}
    	return line;
	}

	/* You can modify criticalAfter() to receive the required arguments */
	public static void criticalAfter() throws IOException {
		
		System.out.println(Thread.activeCount()+" threads are alive");
		System.out.println(curr_event_index +"     "+allTraceEvents.length);
		String curr_line = findNextEventInTrace();
		if(curr_line == null)
			return;
		curr_event = curr_line.split(",")[0];
		synchronized (lock) {
			lock.notifyAll();
			System.out.println(tCustomId[(int)Thread.currentThread().getId()]+" notified all");
		}
	}
       
}