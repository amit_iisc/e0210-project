package pop_replay;

public class ProgramConstants {
	
	public static final String EXIT_METHOD = "<java.lang.System: void exit(int)>";
	public static final String REF_PRINT = "java.io.PrintStream";
	public static final String REF_THREAD = "java.lang.Thread";
	public static final String REF_STRING = "java.lang.String";
	public static final String REF_STRINGBUILDER = "java.lang.StringBuilder";
	public static final String PRINT_METHOD = "<java.io.PrintStream: void println(int)>";
	public static final String THREADID_METHOD = "<java.lang.Thread: long getId()>";
	public static final String CURRENTTHREAD_METHOD = "<java.lang.Thread: java.lang.Thread currentThread()>";
	public static final String ACTIVECOUNT_METHOD = "<java.lang.Thread: int activeCount()>";
	public static final String PRINT_VAR = "<java.lang.System: java.io.PrintStream out>";
	public static final String TESTCASES = "Testcases";
	public static final String INTERMEDIATE_FILES = "Intermediate_Results";
	public static final String DEL = "/";
	public static final String METHOD_INDEX = "methodIndexes";
	public static final String APPENDSTRING_METHOD = "<java.lang.StringBuilder: java.lang.StringBuilder append(java.lang.String)>";
	public static final String APPENDINT_METHOD = "<java.lang.StringBuilder: java.lang.StringBuilder append(java.lang.String)>";
	public static final String SBINIT_METHOD = "<java.lang.StringBuilder: void <init>()>";
	public static final String STRINGINIT_METHOD = "<java.lang.String: void <init>()>";
}
